# Vicord

Vicord is a from-scratch cross platform Discord client for privacy and speed.

## **Vicord is very early in development, everything you see here is just planned/conceptual**

Vicord has Desktop, Mobile, and Web clients. 
- Desktop has the following downloads:
    - Tauri (Recommended)
    - GluonJS
    - Electron
- Mobile only supports Tauri
- There is only the main web client

All clients come with 2 modes
- Normal mode: makes direct communications with Discord servers
- Proxy mode: proxies all Discord traffic through a servers